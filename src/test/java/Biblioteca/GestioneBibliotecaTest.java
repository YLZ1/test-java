package Biblioteca;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class GestioneBibliotecaTest {

    Libro libro1;
    Libro libro2;

    List<Libro> listaLibri;

    @BeforeEach
    void setUp1(){
        libro1 = new Libro("Il nome della rosa", "Umberto Eco", 1980);
        libro2 = new Libro("Il cimitero di Praga", "Umberto Eco", 2010);
    }

    @BeforeEach
    void setUp2(){ listaLibri = new ArrayList<>(); }

    @Test
    void verificaAggiunta(){
        assertEquals(0, listaLibri.size());
        GestioneBiblioteca.aggiungiLibro(listaLibri, libro1);
        assertEquals(1, listaLibri.size());

    }

    @Test
    void verificaRicerca(){
        GestioneBiblioteca.aggiungiLibro(listaLibri, libro1);
        assertEquals(0,GestioneBiblioteca.ricercaLibro(listaLibri,libro1));
        assertNotEquals(1,GestioneBiblioteca.ricercaLibro(listaLibri,libro2));
    }
}