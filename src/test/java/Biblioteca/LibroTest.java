package Biblioteca;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LibroTest {

    Libro libro;

    @BeforeEach
    void setUp(){
        libro = new Libro("Il nome della rosa", "Umberto Eco", 1980);
    }

    @Test
    void verificaProprieta(){
        assertAll("Proprieta' libro",
                ()-> assertNotNull(libro),
                ()-> assertEquals("Il nome della rosa", libro.getTitolo()),
                ()-> assertEquals("Umberto Eco", libro.getAutore()),
                ()-> assertEquals(1980, libro.getAnno()));
    }


}