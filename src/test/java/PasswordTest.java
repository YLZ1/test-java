import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PasswordTest {
    @Test
    void test_assertTrue(){
        try {
            assertTrue(Password.isPassAcc("Sabce197b"));
            assertFalse(Password.isPassAcc("An34"));
            assertFalse(Password.isPassAcc("an34"));
            assertFalse(Password.isPassAcc("an"));
            assertFalse(Password.isPassAcc("abce197b"));
            assertFalse(Password.isPassAcc("SABCDE197B"));
            assertFalse(Password.isPassAcc("SABCDEfgh"));
            assertFalse(Password.isPassAcc("SABCDEFGH"));
            assertFalse(Password.isPassAcc("sabcdefgh"));
            assertTrue(Password.isPassAcc("sabcDE12!"));
            assertFalse(Password.isPassAcc("!!!???,,,"));
            assertFalse(Password.isPassAcc(""));
            assertFalse(Password.isPassAcc(null));
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}