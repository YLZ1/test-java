package eCommerce;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

class ProdottoTest {

    Prodotto prodotto1;
    Prodotto prodotto2;
    Prodotto prodotto3;
    Promozione promozione;
    HashMap<Prodotto,Integer> prodotti;
    Carrello carrello;
    GestioneInventario inventario;


    @BeforeEach
    void setUp() {
        prodotto1 = new Prodotto("1", "Camicia", 10, 2);
        prodotto2 = new Prodotto("2", "Pantaloni", 20, 3);
        prodotto3 = new Prodotto("3", "Calze", 5, 6);
        promozione = new Promozione("Esempio", "Promozione", 0.5);
        prodotti = new HashMap<>();
        carrello = new Carrello(prodotti, promozione);
        inventario = new GestioneInventario(prodotti);
    }

    @Test
    void testProdottoEPromozione(){
        assertNotNull(prodotto1);
        assertAll("Proprieta' prodotto2",
                ()-> assertNotNull(prodotto2),
                ()-> assertEquals("2", prodotto2.getId()),
                ()-> assertEquals("Pantaloni", prodotto2.getNome()),
                ()-> assertEquals(20, prodotto2.getPrezzo()),
                ()-> assertEquals(3,prodotto2.getQuantitaInventario()));
        assertNotNull(promozione);
    }

    @Test
    void testAggiungiERimuoviProdotto(){
        assertEquals(0,carrello.getProdotti().size());
        carrello.aggiungiProdotto(prodotto1,prodotto1.getQuantitaInventario());
        carrello.aggiungiProdotto(prodotto2,prodotto2.getQuantitaInventario());
        carrello.aggiungiProdotto(prodotto3,prodotto3.getQuantitaInventario());
        assertEquals(3,carrello.getProdotti().size());
        carrello.rimuoviProdotto(prodotto3);
        assertEquals(2,carrello.getProdotti().size());
    }
    @Test
    void testAggiornaQuantita(){
        assertEquals(0,inventario.getProdotti().size());
        inventario.aggiornaQuantita(prodotto3,2);
        assertEquals(1,inventario.getProdotti().size());
        assertTrue(inventario.verificaDisponibilita(prodotto3));
        assertFalse(inventario.verificaDisponibilita(prodotto2));
    }
}