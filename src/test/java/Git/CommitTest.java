package Git;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CommitTest {
    Commit commit;

    @BeforeEach
    void setUp(){
        commit = new Commit("ID63774", "Messaggio");
    }

    @Test
    void verificaProprieta(){
        assertAll("Proprieta' commit",
                ()-> assertNotNull(commit),
                ()-> assertEquals("ID63774", commit.getIdCommit()),
                ()-> assertEquals("Messaggio", commit.getMessage()));
    }

}
