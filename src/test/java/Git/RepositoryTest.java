package Git;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class RepositoryTest {
    ArrayList<Commit> list;
    String nome = "Nome";
    Repository repository;
    Commit commit1;
    Commit commit2;

    @BeforeEach
    void setUp(){
        repository = new Repository(nome);
        commit1 = new Commit("ID63774", "Messaggio");
        commit2 = new Commit("ID63735", "Provaprovaprova");
    }

    @Test
    void verificaAggiunta(){
        assertEquals(0,repository.numberOfCommits());
        repository.addCommit(commit1);
        assertEquals(1,repository.numberOfCommits());
    }

    @Test
    void testUltCom(){
        repository.addCommit(commit1);
        assertEquals(commit1,repository.lastCommit());
        repository.addCommit(commit2);
        assertEquals(commit2,repository.lastCommit());
    }

    @Test
    void verificaNumCom(){
        assertEquals(0,repository.numberOfCommits());
        repository.addCommit(commit1);
        assertEquals(1,repository.numberOfCommits());
        repository.addCommit(commit2);
        assertEquals(2,repository.numberOfCommits());
        repository.getCommitList().remove(commit2);
        assertEquals(1,repository.numberOfCommits());
    }
}