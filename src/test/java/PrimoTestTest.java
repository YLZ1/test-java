import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PrimoTestTest {
    @Test
    void test_assertTrue1(){
        assertTrue(PrimoTest.pariOdispari(6,4));
        assertFalse(PrimoTest.pariOdispari(3,5));
        assertTrue(PrimoTest.pariOdispari(7,2));
        assertTrue(PrimoTest.pariOdispari(8,3));
        assertFalse(PrimoTest.pariOdispari(3,4));
        assertFalse(PrimoTest.pariOdispari(4,5));
    }

}