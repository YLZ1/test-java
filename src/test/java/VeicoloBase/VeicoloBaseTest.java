package VeicoloBase;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class VeicoloBaseTest {
    Accessorio accessorio1;
    Accessorio accessorio2;
    Accessorio accessorio3;
    Accessorio accessorio4;
    ArrayList<Accessorio> listaAccessori;
    VeicoloBase veicolo;

    /**
     * Definisco e inizializzo alcuni accessori e un veicolo per i test
     */
    @BeforeEach
    void setUp(){
        accessorio1 = new Accessorio("Cerchi in lega", 200);
        accessorio2 = new Accessorio("GPS", 70);
        accessorio3 = new Accessorio("Ruote 4 stagioni", 400);
        accessorio4 = new Accessorio("AC", 100);
        listaAccessori = new ArrayList<>();
        veicolo = new VeicoloBase("Toyota Cresta", 600000, listaAccessori, 0, 3, 15000);
    }

    /**
     * Test velocita' massima
     */

    @Test
    void testCalcolaVelocitaMassima(){
        assertEquals(100,veicolo.calcolaVelocitaMassima());
    }

    /**
     * Test costo totale
     * @throws Exception
     */

    @Test
    void testCalcolaCostoTotale() throws Exception{
        veicolo.aggiungiAccessorio(accessorio1);
        veicolo.aggiungiAccessorio(accessorio3);
        veicolo.aggiungiAccessorio(accessorio4);
        assertEquals(15700,veicolo.calcolaCostoTotale());
    }
}