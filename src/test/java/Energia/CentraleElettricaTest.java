package Energia;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class CentraleElettricaTest {

    CentraleElettrica centraleElettrica;
    ArrayList<RisorsaEnergetica> listaRisorse;
    RisorsaEnergetica risorsaEnergetica1;
    RisorsaEnergetica risorsaEnergetica2;
    RisorsaEnergetica risorsaEnergetica3;
    RisorsaEnergetica risorsaEnergetica4;

    /**
     * Dichiaro e inizializzo risorse e una centrale per i test
     */

    @BeforeEach
    void setUp(){
        risorsaEnergetica1 = new RisorsaEnergetica(TipoRisorsa.URANIO, 100, 50, 30);
        risorsaEnergetica2 = new RisorsaEnergetica(TipoRisorsa.CARBONE, 500, 10, 20);
        risorsaEnergetica3 = new RisorsaEnergetica(TipoRisorsa.GAS_NATURALE, 1000, 20, 10);
        risorsaEnergetica4 = new RisorsaEnergetica(TipoRisorsa.URANIO, 100, 50, 30);
        listaRisorse = new ArrayList<>();
        centraleElettrica = new CentraleElettrica(listaRisorse, 0, 0.5);
    }

    /**
     * Test per il metodo calcolaProduzioneEnergia
     * @throws Exception
     */

    @Test
    void testCalcolaProduzioneEnergia() throws Exception{ //Nel caso in cui una centrale non ha risorse, viene stampata l'Exception
        centraleElettrica.aggiungiRisorsa(risorsaEnergetica1);
        centraleElettrica.aggiungiRisorsa(risorsaEnergetica2);
        assertEquals(5000, centraleElettrica.calcolaProduzioneEnergia());
    }

    /**
     * Test per il metodo simulaCosto
     * @throws Exception
     */

    @Test
    void testSimulaCosto() throws Exception{
        centraleElettrica.aggiungiRisorsa(risorsaEnergetica1);
        centraleElettrica.aggiungiRisorsa(risorsaEnergetica2);

        assertEquals(3400, centraleElettrica.simulaCosto(2600));
    }


}