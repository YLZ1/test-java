package Git;

public class Commit {
    private String idCommit;
    private String message;

    public Commit(String idCommit, String message) {
        this.idCommit = idCommit;
        this.message = message;
    }

    public String getIdCommit() {
        return idCommit;
    }

    public String getMessage() {
        return message;
    }

    public void setIdCommit(String idCommit) {
        this.idCommit = idCommit;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
