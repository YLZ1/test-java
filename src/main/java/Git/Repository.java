package Git;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Repository {
    private String nome;
    private List<Commit> commitList;

    public Repository(String nome) {
        this.nome = nome;
        commitList = new ArrayList<>();
    }

    public String getNome() {
        return nome;
    }

    public List<Commit> getCommitList() {
        return commitList;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setCommitList(List<Commit> commitList) {
        this.commitList = commitList;
    }

    public void addCommit(Commit newCommit){
        this.commitList.add(newCommit);
    }

    public Commit lastCommit(){
        if(this.commitList.isEmpty()){ return null; }
        return this.commitList.get(this.commitList.size()-1);
    }

    public int numberOfCommits(){
        return this.commitList.size();
    }

}
