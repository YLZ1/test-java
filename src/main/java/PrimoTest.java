public class PrimoTest {
    public static boolean pariOdispari(int n, int m){
        if ( (n%2) == 0 && (m%2) ==0 )
            return true;
        else if ( (n%2) == 1 && (m%2) == 1 )
            return false;
        else if ( (n%2) == 1 && (m%2) == 0 && n>m )
            return true;
        else if ( (n%2) == 0 && (m%2) == 1 && n>m )
            return true;
        else if ( (n%2) == 1 && (m%2) == 0 && n<=m )
            return false;
        else
            return false;
    }

}
