package Energia;

/**
 * @author Lorenzo
 * @version 1.0
 * La classe RisorsaEnergetica rappresenta una risorsa usata per generare energia in una centrale elettrica.
 * E' rappresentata da un tipo di risorsa, la quantita' disponibile, la potenza calorifica e il prezzo
 */

public class RisorsaEnergetica {
    private TipoRisorsa tipoRisorsa;
    private double quantitaDisponibile;     //MISURATO IN Kg
    private double potenzaCalorifica;       //MISURATO in KWh/Kg
    private double prezzo;

    /**
     * Il metodo restituisce il tipo di risorsa
     * @return tipoRisorsa
     */

    public TipoRisorsa getTipoRisorsa() {
        return tipoRisorsa;
    }

    /**
     * Il metodo restituisce la quantita' disponibile
     * @return quantitaDisponibile
     */

    public double getQuantitaDisponibile() {
        return quantitaDisponibile;
    }

    /**
     * Il metodo setta la quantita' disponibile
     * @param quantitaDisponibile
     */

    public void setQuantitaDisponibile(double quantitaDisponibile) {
        this.quantitaDisponibile = quantitaDisponibile;
    }

    /**
     * Il metodo restituisce la potenza calorifica
     * @return potenzaCalorifica
     */

    public double getPotenzaCalorifica() {
        return potenzaCalorifica;
    }

    /**
     * Il metodo restituisce il prezzo
     * @return prezzo
     */

    public double getPrezzo() {
        return prezzo;
    }

    /**
     * Il metodo inizializza un oggetto ti tipo RisorsaEnergetica
     * @param tipoRisorsa
     * @param quantitaDisponibile
     * @param potenzaCalorifica
     * @param prezzo
     */
    public RisorsaEnergetica(TipoRisorsa tipoRisorsa,
                             double quantitaDisponibile,
                             double potenzaCalorifica,
                             double prezzo){
        this.tipoRisorsa = tipoRisorsa;
        this.quantitaDisponibile = quantitaDisponibile;
        this.potenzaCalorifica = potenzaCalorifica;
        this.prezzo = prezzo;
    }

    /**
     * Il metodo calcola e restituisce il costo a partire da una quantita' di materiale
     * @param quantita
     * @return costo
     */

    public double calcolaCosto(double quantita){
        return quantita * this.prezzo;
    }

    /**
     * Il metodo stampa una descrizione della risorsa
     */
    public void stampaRisorsa(){
        System.out.println("Tipo di risorsa: "+this.getTipoRisorsa()+"\t" +
                "quantita' disponibile: "+this.getQuantitaDisponibile()+"\t" +
                "potenza calorifica: "+this.getPotenzaCalorifica()+"\t" +
                "prezzo per unita': "+this.getPrezzo());
    }

    /**
     * Metodo che calcola e restituisce la quantita' di energia (espressa in KWh) prodotta dalla risorsa
     * @return quantitaEnergia
     */
    public double calcolaQuantitaEnergia(){
        return this.getQuantitaDisponibile()*this.getPotenzaCalorifica();
    }
}