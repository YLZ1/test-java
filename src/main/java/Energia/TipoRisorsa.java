package Energia;

public enum TipoRisorsa {
    URANIO,
    CARBONE,
    GAS_NATURALE,
}
