package Energia;

import java.util.ArrayList;

/**
 * @author Lorenzo
 * @version 1.0
 * La classe CentraleElettrica rappresenza una centrale elettrica.
 * Gli attributi sono un array di risorse, un numero di risorse e il tasso di efficienza
 */

public class CentraleElettrica {
    private ArrayList<RisorsaEnergetica> listaRisorse;
    private int numRisorse;
    private double efficienza;

    /**
     * Il metodo restituisce l'efficienza
     * @return efficienza
     */
    public double getEfficienza() {
        return efficienza;
    }

    /**
     * Il metodo inizializza una CentraleEnergetica
     * @param listaRisorse
     * @param numRisorse
     * @param efficienza
     */
    public CentraleElettrica(ArrayList<RisorsaEnergetica> listaRisorse, int numRisorse, double efficienza){
        this.listaRisorse = listaRisorse;
        this.numRisorse = numRisorse;
        this.efficienza = efficienza;
    }

    /**
     * Il metodo aggiunge la risorsa passata come parametro all'array delle risorse
     * @param risorsa
     */
    public void aggiungiRisorsa(RisorsaEnergetica risorsa){
        this.listaRisorse.add(risorsa);
        this.numRisorse++;
    }

    /**
     * Il metodo stampa l'elenco delle risorse secondo le specifiche del metodo stampaRisorsa
     */

    public void stampaRisorse(){
        System.out.println("Lista risorse:\n");
        for ( int i=0; i<this.numRisorse; i++ )
            this.listaRisorse.get(i).stampaRisorsa();
        System.out.println("\n");
    }

    /**
     * Metodo per calcolare la quantita' di energia prodotta dalla centrale tenendo in considerazione l'efficienza
     * @return energia
     */
    public double calcolaProduzioneEnergia() throws Exception{
        double energia = 0;

        if ( this.numRisorse == 0 )
            throw new Exception("Centrale senza risorse");

        for ( int i=0; i<this.numRisorse; i++ )
            energia += this.listaRisorse.get(i).calcolaQuantitaEnergia();
        return energia*this.efficienza;
    }

    /**
     * Il metodo simula un consumo di energia da parte della centrale
     * @param consumo
     */
    public void simulaConsumo(double consumo) throws Exception{
        int i = 0;
        double energiaDiUnaRisorsa;

        //Eccezione: risorse insufficienti
        if ( consumo > this.calcolaProduzioneEnergia() )
            throw new Exception("La centrale non puo' produrre una tale quantita' di energia");

        //Finche' ci sono risorse e la centrale non ha fatto fronte al consumo...
        while ( i<this.numRisorse && consumo > 0 ){
            //Calcolo l'energia di una risorsa
            energiaDiUnaRisorsa = this.listaRisorse.get(i).calcolaQuantitaEnergia()*this.efficienza;
            //Se tale energia e' tale da far fronte al consumo, allora...
            if( energiaDiUnaRisorsa - consumo >= 0 ){ //modifico la quantita' di materiale che ho
                this.listaRisorse.get(i).setQuantitaDisponibile( this.listaRisorse.get(i).getQuantitaDisponibile() - (consumo/this.listaRisorse.get(i).getPotenzaCalorifica()) );
                consumo = 0;
            }
            else{   //Altrimenti, se l'energia di una risorsa e' insufficiente,...
                consumo = consumo - energiaDiUnaRisorsa;    //...ricalcolo il consumo tenendo conto dell'energia che ho generato
                //Poi elimino la risorsa usaurita
                this.numRisorse--;
                this.listaRisorse.remove(i);
                //Passo alla risorsa successiva
                i++;
            }
        }
    }

    /**
     * Il metodo calcola il costo del consumo di energia da parte della centrale
     * @param consumo
     */

    public double simulaCosto(double consumo) throws Exception{
        int i = 0;
        double energiaDiUnaRisorsa;
        double prezzo = 0;  //output
        double resto;       //resto e' la quantita di energia spesa da una singola risorsa per far fronte al consumo

        //Eccezione: risorse insufficienti
        if ( consumo > this.calcolaProduzioneEnergia() )
            throw new Exception("La centrale non puo' produrre una tale quantita' di energia");

        //Simile al metodo simulaConsumo
        while ( i<this.numRisorse && consumo > 0 ){
            energiaDiUnaRisorsa = this.listaRisorse.get(i).calcolaQuantitaEnergia()*this.efficienza;
            if( energiaDiUnaRisorsa - consumo >= 0 ){
                double div = this.getEfficienza() * this.listaRisorse.get(i).getPotenzaCalorifica();
                resto = consumo/div;
                prezzo += this.listaRisorse.get(i).calcolaCosto(resto);
                consumo = 0;
            }
            else{
                consumo = consumo - energiaDiUnaRisorsa;
                resto = this.listaRisorse.get(i).calcolaQuantitaEnergia()/this.listaRisorse.get(i).getPotenzaCalorifica();
                prezzo += this.listaRisorse.get(i).calcolaCosto(resto);
                i++;
            }
        }
        return prezzo;
    }
}