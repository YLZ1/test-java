package eCommerce;

import java.util.HashMap;

public class Ordine {
    private String idOrdine;
    private HashMap<Prodotto,Integer> prodotti;
    private Stato stato;
    private double totaleOrdine;

    public String getIdOrdine() {
        return idOrdine;
    }

    public void setIdOrdine(String idOrdine) {
        this.idOrdine = idOrdine;
    }

    public HashMap<Prodotto, Integer> getProdotti() {
        return prodotti;
    }

    public void setProdotti(HashMap<Prodotto,Integer> prodotti) {
        this.prodotti = prodotti;
    }

    public Stato getStato() {
        return stato;
    }

    public void setStato(Stato stato) {
        this.stato = stato;
    }

    public double getTotaleOrdine() {
        return totaleOrdine;
    }

    public void setTotaleOrdine(double totaleOrdine) {
        this.totaleOrdine = totaleOrdine;
    }
    public Ordine(String idOrdine, HashMap<Prodotto,Integer> prodotti, Stato stato, double totaleOrdine){
        this.idOrdine = idOrdine;
        this.prodotti = prodotti;
        this.stato = stato;
        this.totaleOrdine = totaleOrdine;
    }

    public void aggiungiProdotto(Prodotto prodotto, int quantita){
        this.prodotti.put(prodotto,quantita);
    }

    public void rimuoviProdotto(Prodotto prodotto){
        this.prodotti.remove(prodotto);
    }

    public void calcolaTotale(){
        double tot = 0;
        for (HashMap.Entry<Prodotto, Integer> set :
                this.prodotti.entrySet()) {
            tot += set.getKey().getPrezzo()*set.getValue();
        }

        this.setTotaleOrdine(tot);
    }

    public void aggiornaStato(Stato stato){
        this.setStato(stato);
    }
}
