package eCommerce;

public class Promozione {
    private String codicePromozione;
    private String descrizione;
    private double percentualeSconto;

    public String getCodicePromozione() {
        return codicePromozione;
    }

    public void setCodicePromozione(String codicePromozione) {
        this.codicePromozione = codicePromozione;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public double getPercentualeSconto() {
        return percentualeSconto;
    }

    public void setPercentualeSconto(double percentualeSconto) {
        this.percentualeSconto = percentualeSconto;
    }
    public Promozione(String codicePromozione, String descrizione, double percentualeSconto){
        this.codicePromozione = codicePromozione;
        this.descrizione = descrizione;
        this.percentualeSconto = percentualeSconto;
    }

    public void applicaSconto(Ordine ordine, Promozione promozione){
        double nuovoPrezzo = ordine.getTotaleOrdine() - ordine.getTotaleOrdine()*promozione.getPercentualeSconto();
        ordine.setTotaleOrdine( nuovoPrezzo );
    }
}
