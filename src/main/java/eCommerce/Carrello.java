package eCommerce;

import java.util.HashMap;

public class Carrello {
    private HashMap<Prodotto, Integer> prodotti;
    private Promozione promozione;

    public HashMap<Prodotto, Integer> getProdotti() {
        return prodotti;
    }

    public void setProdotti(HashMap<Prodotto, Integer> prodotti) {
        this.prodotti = prodotti;
    }

    public Promozione getPromozione() {
        return promozione;
    }

    public void setPromozione(Promozione promozione) {
        this.promozione = promozione;
    }
    public Carrello(HashMap<Prodotto,Integer> prodotti, Promozione promozione){
        this.prodotti = prodotti;
        this.promozione = promozione;
    }

    public void aggiungiProdotto(Prodotto prodotto, int quantita){
        this.prodotti.put(prodotto,quantita);
    }
    public void rimuoviProdotto(Prodotto prodotto){
        this.prodotti.remove(prodotto);
    }

    public void applicaPromozione(Promozione promozione){
        this.setPromozione(promozione);
    }

    public double calcolaTotNoPromo(){
        double tot = 0;
        for (HashMap.Entry<Prodotto, Integer> set :
                this.prodotti.entrySet()) {
            tot += set.getKey().getPrezzo()*set.getValue();
        }
        return tot;
    }
    public double calcolaTotPromo(){
        double tot = this.calcolaTotNoPromo();
        return tot - tot*this.promozione.getPercentualeSconto();
    }

}
