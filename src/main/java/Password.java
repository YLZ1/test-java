public class Password {

    public static boolean containsDigit(String password){
        for ( int i=0; i<password.length(); i++ )
            if ( Character.isDigit(password.charAt(i)) )
                return true;
        return false;
    }

    public static boolean containsUpperCase(String password){
        for ( int i=0; i<password.length(); i++ )
            if ( Character.isUpperCase(password.charAt(i)) )
                return true;
        return false;
    }

    public static boolean containsLowerCase(String password){
        for ( int i=0; i<password.length(); i++ )
            if ( Character.isLowerCase(password.charAt(i)) )
                return true;
        return false;
    }
    public static boolean isPassAcc(String password) throws Exception{
        if ( password == null ){
            throw new Exception("Inserita una stringa vuota");
        }
        if ( password.length() >= 8 &&
                containsDigit(password) &&
                containsLowerCase(password) &&
                containsUpperCase(password) )
            return true;
        return false;
    }
}
