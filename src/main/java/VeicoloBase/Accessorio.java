package VeicoloBase;

/**
 * @author Lorenzo
 * @version 1.0
 * La classe Accessorio rappresenta un accessorio di un veicolo. Un accessorio e' caratterizzato da un nome e da un prezzo
 */

public class Accessorio {
    private String nome;
    private double prezzo;
    /**
     * Il metodo restituisce il nome
     * @return nome
     */
    public String getNome() {
        return nome;
    }
    /**
     * Il metodo setta il nome
     * @param nome
     */
    public void setNome(String nome) {
        this.nome = nome;
    }
    /**
     * Il metodo restituisce il prezzo
     * @return prezzo
     */
    public double getPrezzo() {
        return prezzo;
    }
    /**
     * Il metodo setta il prezzo
     * @param prezzo
     */
    public void setPrezzo(double prezzo) {
        this.prezzo = prezzo;
    }
    /**
     * Il metodo inizializza un accessorio
     * @param nome
     * @param prezzo
     */
    public Accessorio(String nome, double prezzo){
        this.nome = nome;
        this.prezzo = prezzo;
    }
    /**
     * Il metodo stampa una descrizione dell'accessorio
     */
    public void stampaAccessorio(){
        System.out.println("Accessorio: "+this.getNome()+"\t"+
                            "prezzo: "+this.getPrezzo());
    }
}