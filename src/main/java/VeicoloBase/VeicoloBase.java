package VeicoloBase;

import java.util.ArrayList;
import java.util.Random;

/**
 * @author Lorenzo
 * @version 1.0
 * La classe veicolo base descrive un veicolo tramite gli attributi tipoVeicolo, potenzaMotore, listaAccessori, numAccessori e prezzo
 */
public class VeicoloBase {
    private String tipoVeicolo;
    private int potenzaMotore;
    private ArrayList<Accessorio> listaAccessori;
    private int numAccessori;
    private int numMaxAccessori; //numero massimo di accessori stabilito dal costruttore
    private double prezzo;
    /**
     * Il metodo restituisce il tipo di veicolo
     * @return tipoVeicolo
     */
    public String getTipoVeicolo() {
        return tipoVeicolo;
    }
    /**
     * Il metodo setta il tipo di veicolo
     * @param tipoVeicolo
     */
    public void setTipoVeicolo(String tipoVeicolo) {
        this.tipoVeicolo = tipoVeicolo;
    }
    /**
     * Il metodo restituisce la potenza del motore
     * @return potenzaMotore
     */
    public int getPotenzaMotore() {
        return potenzaMotore;
    }
    /**
     * Il metodo setta la potenza del motore
     * @param potenzaMotore
     */
    public void setPotenzaMotore(int potenzaMotore) {
        this.potenzaMotore = potenzaMotore;
    }
    /**
     * Il metodo restituisce la lista di accessori
     * @return listaAccessori
     */
    public ArrayList<Accessorio> getListaAccessori() {
        return listaAccessori;
    }
    /**
     * Il metodo setta la lista degli accessori
     * @param listaAccessori
     */
    public void setListaAccessori(ArrayList<Accessorio> listaAccessori) {
        this.listaAccessori = listaAccessori;
    }
    /**
     * Il metodo restituisce il numero di accessori
     * @return numAccessori
     */
    public int getNumAccessori() {
        return numAccessori;
    }
    /**
     * Il metodo setta il numero di accessori
     * @param numAccessori
     */
    public void setNumAccessori(int numAccessori) {
        this.numAccessori = numAccessori;
    }
    /**
     * Il metodo restituisce il numero massimo di accessori
     * @return numMaxAccessori
     */
    public int getNumMaxAccessori() {
        return numMaxAccessori;
    }
    /**
     * Il metodo setta il numero massimo di accessori
     * @param numMaxAccessori
     */
    public void setNumMaxAccessori(int numMaxAccessori) {
        this.numMaxAccessori = numMaxAccessori;
    }
    /**
     * Il metodo restituisce il prezzo
     * @return prezzo
     */
    public double getPrezzo() {
        return prezzo;
    }
    /**
     * Il metodo setta il prezzo
     * @param prezzo
     */
    public void setPrezzo(double prezzo) {
        this.prezzo = prezzo;
    }
    /**
     * Il metodo inizializza un TipoVeicolo
     * @param tipoVeicolo
     * @param potenzaMotore
     * @param listaAccessori
     * @param numAccessori
     * @param numMaxAccessori
     * @param prezzo
     */
    public VeicoloBase(String tipoVeicolo,
                       int potenzaMotore,
                       ArrayList<Accessorio> listaAccessori,
                       int numAccessori,
                       int numMaxAccessori,
                       double prezzo){
        this.tipoVeicolo = tipoVeicolo;
        this.potenzaMotore = potenzaMotore;
        this.listaAccessori = listaAccessori;
        this.numAccessori = numAccessori;
        this.numMaxAccessori = numMaxAccessori;
        this.prezzo = prezzo;
    }
    /**
     * Il metodo aggiunge un accessorio alla lista di accessori
     * @param accessorio
     */
    public void aggiungiAccessorio(Accessorio accessorio) throws Exception{
        if ( this.listaAccessori.size()+1 > this.numMaxAccessori )
            throw new Exception("Non e' possibile installare ulteriori accessori sul veicolo "+this.getTipoVeicolo());
        this.listaAccessori.add(accessorio);
        this.setNumAccessori(this.listaAccessori.size());
    }
    /**
     * Il metodo calcola la velocita' massima.
     * @return velocita' massima
     */
    /*
    * La formula e' giustificata nel modo seguente:
    * la potenza P = dW/dt e W=F scalar s. Se F=cost, dW=F*ds, quindi
    * P=F* ds/dt = F*v = m*a*v. v = P/m*a. Se la macchina pesa 2000kg e
    * accellera a 3 m/s^2 allora v = Potenza / 6000
    * */
    public double calcolaVelocitaMassima(){
        return this.potenzaMotore/6000;
    }
    /**
     * Il metodo stampa la lista completa degli accessori del veicolo
     */
    public void stampaListaAccessori(){
        for ( int i=0; i<this.getNumAccessori(); i++ )
            this.getListaAccessori().get(i).stampaAccessorio();
        System.out.println("\n");
    }
    /**
     * Il metodo restituisce il prezzo totale degli accessori del veicolo
     * @return prezzoAccessori
     */
    public double prezzoTotaleAccessori(){
        double prezzoAccessori = 0;
        for ( int i=0; i<this.getNumAccessori(); i++ )
            prezzoAccessori += this.getListaAccessori().get(i).getPrezzo();
        return prezzoAccessori;
    }
    /**
     * Il metodo restituisce il prezzo totale del veicolo, accessori inclusi
     * @return prezzo totale
     */
    public double calcolaCostoTotale(){
        return this.getPrezzo() + this.prezzoTotaleAccessori();
    }
    /**
     * Il metodo genera randomicamente e restuisce una velocita' media
     * @return velocitaMedia
     */
    public double calcolaVelocitaMedia(){
        Random r = new Random();
        int low = 1;
        int high = 100;
        return r.nextInt(high-low) + low;
    }
    /**
     * Il metodo simula un viaggio stampando una breve descrizione di questo
     * @param distanza
     * @param consumoMedio
     */
    public void simulaViaggio(double distanza, double consumoMedio){
        double velocitaMedia = calcolaVelocitaMedia();
        double tempo = velocitaMedia * distanza;
        double consumoTotale = consumoMedio * distanza;
        System.out.println("Simulazione viaggio:\n" +
                "Distanza= "+distanza+"\t" +"tempo: "+tempo+"\t"+"consumo totale: "+consumoTotale);
        System.out.println("\n");
    }
}