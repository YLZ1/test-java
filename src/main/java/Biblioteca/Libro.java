package Biblioteca;

public class Libro {
    private String titolo;
    private String autore;
    private int anno;
    public String getTitolo(){
        return this.titolo;
    }
    public String getAutore(){
        return this.autore;
    }
    public int getAnno(){
        return this.anno;
    }
    public void setTitolo(String titolo){
        this.titolo = titolo;
    }
    public void setAutore(String autore){
        this.autore = autore;
    }
    public  void setAnno(int anno){
        this.anno = anno;
    }
    public Libro(String titolo, String autore, int anno){
        this.titolo = titolo;
        this.autore = autore;
        this.anno = anno;
    }
    public static void stampaLibro(Libro libro){
        System.out.println("Titolo: "+libro.getTitolo()+" - "+"autore: "+libro.getAutore()+" - "+"anno: "+libro.getAnno());
    }
}
