package Biblioteca;

import java.util.List;

public class GestioneBiblioteca {
    public static void aggiungiLibro(List<Libro> listaLibri, Libro libro1){
        listaLibri.add(libro1);
    }
    public static int ricercaLibro(List<Libro> listaLibri, Libro libro1){
        return listaLibri.indexOf(libro1);
    }
    public static void stampaLista(List<Libro> listaLibri){
        System.out.println("Lista libri:");
        for ( int i=0; i<listaLibri.size(); i++ )
            listaLibri.get(i).stampaLibro(listaLibri.get(i));
        System.out.println("\n");
    }
    public static int numeroPagine(List<Libro> listaLibro){
        int res = 0;

        for ( int i=0; i<listaLibro.size(); i++ )
        {
            if (listaLibro.get(i) instanceof LibroStampato)
                res += ((LibroStampato) listaLibro.get(i)).getnPagine();
        }
        return res;
    }
}