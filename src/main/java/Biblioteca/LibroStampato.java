package Biblioteca;

public class LibroStampato extends Libro{
    private int nPagine;
    public int getnPagine() {
        return nPagine;
    }
    public void setnPagine(int nPagine) {
        this.nPagine = nPagine;
    }
    public  LibroStampato(String titolo, String autore, int anno, int nPagine){
        super(titolo, autore, anno);
        this.nPagine = nPagine;
    }
}
